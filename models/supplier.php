<?php
	include "connection.php";
	$action = $_GET['action'];

	switch($action) {
		case 'insert':
			$name = $_POST['supplier_name'];
			$address = $_POST['address'];
			
			$query = "SELECT * FROM supplier WHERE name = '$name' AND address = '$address'";
			$execute = mysqli_query($conn, $query);
			if (mysqli_num_rows($execute) > 0) {
				header("location:../views/supplier.php?response=failed&message=duplicate");
			}
			else {
				$query = "INSERT INTO supplier(name,address) VALUES('$name', '$address')";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/supplier.php?response=success");
				}
				else {
					header("location:../views/supplier.php?response=failed&message=sql_error");
				}
			}
			mysqli_close($conn);
			break;

		case 'update':
			// Method update
			function update($conn, $supplier_id, $name, $address) {
				$query = "UPDATE supplier SET name='$name', address='$address' WHERE supplier_id ='$supplier_id'";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/supplier.php?response=success");
				}
				else {
					header("location:../views/supplier.php?response=failed&message=sql_error");
				}
			}

			$supplier_id = $_POST['e_supplier_id'];
			$name = $_POST['e_supplier_name'];
			$address = $_POST['e_address'];
			
			$query = "SELECT * FROM supplier WHERE name = '$name' AND address = '$address'";
			$execute = mysqli_query($conn, $query);

			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$current_id = $result['supplier_id'];
				if ($current_id == $supplier_id) {
					update($conn, $supplier_id, $name, $address);
				}
				else {
					header("location:../views/supplier.php?response=failed&message=duplicate");
				}
			}
			else {
				update($conn, $supplier_id, $name, $address);
			}
			mysqli_close($conn);
			break;
			break;
			
		case 'delete':
			$supplier_id = $_GET['supplier_id'];
			$query = "DELETE FROM supplier WHERE supplier_id = '$supplier_id'";
			$execute = mysqli_query($conn, $query);
			if ($execute) {
				header("location:../views/supplier.php?response=success");
			}
			else {
				header("location:../views/supplier.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
	}
?>