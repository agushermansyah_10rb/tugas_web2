<?php
	include "connection.php";
	$action = $_GET['action'];

	switch($action) {
		case 'insert':
			$merk_id = $_POST['merk_id'];
			$name = $_POST['bname'];
			
			$query = "SELECT * FROM motor_data JOIN merk_motor ON merk_motor.md_id = motor_data.md_id WHERE merk_id = '$merk_id' AND name = '$name'";
			$execute = mysqli_query($conn, $query);
			if ($merk_id == '') {
				header("location:../views/brand.php?response=failed");
			}
			else {
				if (mysqli_num_rows($execute) > 0) {
					header("location:../views/brand.php?response=failed&message=duplicate&merk_id=$merk_id");
				}
				else {
					$query = "INSERT INTO motor_data(name) VALUES('$name')";
					$execute = mysqli_query($conn, $query);
					if ($execute) {
						$brand_id = mysqli_insert_id($conn);
						$query = "INSERT INTO merk_motor(merk_id, md_id) VALUES('$merk_id', '$brand_id')";
						mysqli_query($conn, $query);
						header("location:../views/brand.php?response=success&merk_id=$merk_id");
					}
					else {
						header("location:../views/brand.php?response=failed&message=sql_error&merk_id=$merk_id");
					}
				}
			}
			mysqli_close($conn);
			break;

		case 'update':
			// Method update
			function update($conn, $merk_id, $brand_id, $name) {
				$query = "UPDATE motor_data SET name='$name' WHERE md_id ='$brand_id'";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/brand.php?response=success&merk_id=$merk_id");
				}
				else {
					header("location:../views/brand.php?response=failed&message=sql_error&merk_id=$merk_id");
				}
			}

			$merk_id = $_POST['e_merk_id'];
			$brand_id = $_POST['e_brand_id'];
			$name = $_POST['e_brand_name'];
			
			$query = "SELECT * FROM motor_data WHERE name = '$name'";
			$execute = mysqli_query($conn, $query);

			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$current_id = $result['md_id'];
				if ($current_id == $brand_id) {
					update($conn, $merk_id, $brand_id, $name);
				}
				else {
					header("location:../views/brand.php?response=failed&message=duplicate&merk_id=$merk_id");
				}
			}
			else {
				update($conn, $merk_id, $brand_id, $name);
			}
			mysqli_close($conn);
			break;
			
		case 'delete':
			$merk_id = $_GET['merk_id'];
			$brand_id = $_GET['brand_id'];
			$query = "DELETE FROM motor_data WHERE md_id = '$brand_id'";
			$execute = mysqli_query($conn, $query);
			if ($execute) {
				header("location:../views/brand.php?response=success&merk_id=$merk_id");
			}
			else {
				header("location:../views/brand.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
	}
?>