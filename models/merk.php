<?php
	include "connection.php";
	$action = $_GET['action'];

	switch($action) {
		case 'insert':
			$category_id = $_POST['category_id'];
			$name = $_POST['mname'];
			
			$query = "SELECT * FROM merk JOIN category_merk ON category_merk.merk_id = merk.merk_id WHERE category_id = '$category_id' AND name = '$name'";
			$execute = mysqli_query($conn, $query);
			if ($category_id == '') {
				header("location:../views/merk.php?response=failed");
			}
			else {
				if (mysqli_num_rows($execute) > 0) {
					header("location:../views/merk.php?response=failed&message=duplicate&category_id=$category_id");
				}
				else {
					$query = "INSERT INTO merk(name) VALUES('$name')";
					$execute = mysqli_query($conn, $query);
					if ($execute) {
						$merk_id = mysqli_insert_id($conn);
						$query = "INSERT INTO category_merk(category_id, merk_id) VALUES('$category_id', '$merk_id')";
						mysqli_query($conn, $query);
						header("location:../views/merk.php?response=success&category_id=$category_id");
					}
					else {
						header("location:../views/merk.php?response=failed&message=sql_error&category_id=$category_id");
					}
				}
			}
			mysqli_close($conn);
			break;

		case 'update':
			// Method update
			function update($conn, $category_id, $merk_id, $name) {
				$query = "UPDATE merk SET name='$name' WHERE merk_id ='$merk_id'";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/merk.php?response=success&category_id=$category_id");
				}
				else {
					header("location:../views/merk.php?response=failed&message=sql_error&category_id=$category_id");
				}
			}

			$category_id = $_POST['e_category_id'];
			$merk_id = $_POST['e_merk_id'];
			$name = $_POST['e_merk_name'];
			
			$query = "SELECT * FROM merk WHERE name = '$name'";
			$execute = mysqli_query($conn, $query);

			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$current_id = $result['merk_id'];
				if ($current_id == $merk_id) {
					update($conn, $category_id, $merk_id, $name);
				}
				else {
					header("location:../views/merk.php?response=failed&message=duplicate&category_id=$category_id");
				}
			}
			else {
				update($conn, $category_id, $merk_id, $name);
			}
			mysqli_close($conn);
			break;
			
		case 'delete':
			$category_id = $_GET['category_id'];
			$merk_id = $_GET['merk_id'];
			$query = "DELETE FROM merk WHERE merk_id = '$merk_id'";
			$execute = mysqli_query($conn, $query);
			if ($execute) {
				header("location:../views/merk.php?response=success&category_id=$category_id");
			}
			else {
				header("location:../views/merk.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
	}
?>