<?php
	include "connection.php";
	$action = $_GET['action'];

	switch($action) {
		case 'insert':
			$product_name = $_POST['product_name'];
			$size = $_POST['size'];
			$position = $_POST['position'];
			$tube_type = $_POST['tube_type'];
			$ring_number = $_POST['ring_number'];
			$total = $_POST['total'];
			$price = $_POST['price'];
			$supplier_id = $_POST['supplier_id'];
			
			$query = "SELECT * FROM purchasing WHERE product_name = '$product_name' AND size = '$size' AND position='$position' AND tube_type='$tube_type' AND ring_number='$ring_number' AND supplier_id='$supplier_id'";
			$execute = mysqli_query($conn, $query);
			if (mysqli_num_rows($execute) > 0) {
				header("location:../views/purchasing.php?response=failed&message=duplicate");
			}
			else {
				$query = "INSERT INTO purchasing(product_name,size,position,tube_type,ring_number,total,price,supplier_id) VALUES('$product_name', '$size', '$position', '$tube_type', '$ring_number', '$total', '$price', '$supplier_id')";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/purchasing.php?response=success");
				}
				else {
					// header("location:../views/purchasing.php?response=failed&message=sql_error");
					echo mysqli_error($conn);
				}
			}
			mysqli_close($conn);
			break;

		case 'update':
			// Method update
			function update($conn, $purchasing_id, $product_name, $size, $position, $tube_type, $ring_number, $total, $price, $supplier_id) {
				$query = "UPDATE purchasing SET product_name='$product_name', size='$size', position='$position', tube_type='$tube_type', ring_number='$ring_number', total='$total', price='$price', supplier_id='$supplier_id' WHERE purchasing_id ='$purchasing_id'";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/purchasing.php?response=success");
				}
				else {
					header("location:../views/purchasing.php?response=failed&message=sql_error");
				}
			}

			$purchasing_id = $_POST['purchasing_id'];
			$product_name = $_POST['product_name'];
			$size = $_POST['size'];
			$position = $_POST['position'];
			$tube_type = $_POST['tube_type'];
			$ring_number = $_POST['ring_number'];
			$total = $_POST['total'];
			$price = $_POST['price'];
			$supplier_id = $_POST['supplier_id'];
			
			$query = "SELECT * FROM purchasing WHERE product_name = '$product_name' AND size = '$size' AND position='$position' AND tube_type='$tube_type' AND ring_number='$ring_number' AND supplier_id='$supplier_id'";
			$execute = mysqli_query($conn, $query);

			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$current_id = $result['purchasing_id'];
				if ($current_id == $purchasing_id) {
					update($conn, $purchasing_id, $product_name, $size, $position, $tube_type, $ring_number, $total, $price, $supplier_id);
				}
				else {
					header("location:../views/purchasing.php?response=failed&message=duplicate");
				}
			}
			else {
				update($conn, $purchasing_id, $product_name, $size, $position, $tube_type, $ring_number, $total, $price, $supplier_id);
			}
			mysqli_close($conn);
			break;

		case 'delete':
			$purchasing_id = $_GET['purchasing_id'];
			$query = "DELETE FROM purchasing WHERE purchasing_id = '$purchasing_id'";
			$execute = mysqli_query($conn, $query);
			if ($execute) {
				header("location:../views/purchasing.php?response=success");
			}
			else {
				header("location:../views/purchasing.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
	}
?>