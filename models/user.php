<?php
	include "connection.php";
	$action = $_GET['action'];

	switch($action) {
		case 'insert':
			$name = $_POST['name'];
			$username = $_POST['username'];
			$password = $_POST['password'];
			
			$query = "SELECT * FROM user WHERE username = '$username'";
			$execute = mysqli_query($conn, $query);
			if (mysqli_num_rows($execute) > 0) {
				header("location:../views/user.php?response=failed&message=duplicate");
			}
			else {
				$query = "INSERT INTO user(username,name,password) VALUES('$username', '$name', '$password')";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/user.php?response=success");
				}
				else {
					header("location:../views/user.php?response=failed&message=sql_error");
				}
			}
			mysqli_close($conn);
			break;

		case 'update':
			// Method update
			function update($conn, $user_id, $username, $name, $password) {
				$query = "UPDATE user SET name='$name', username='$username', password='$password' WHERE user_id ='$user_id'";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/user.php?response=success");
				}
				else {
					header("location:../views/user.php?response=failed&message=sql_error");
				}
			}

			$user_id = $_POST['e_user_id'];
			$username = $_POST['e_username'];
			$name = $_POST['e_display_name'];
			$password = $_POST['e_password'];
			
			$query = "SELECT * FROM user WHERE username = '$username'";
			$execute = mysqli_query($conn, $query);

			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$current_id = $result['user_id'];
				if ($current_id == $user_id) {
					update($conn, $user_id, $username, $name, $password);
				}
				else {
					header("location:../views/user.php?response=failed&message=duplicate");
				}
			}
			else {
				update($conn, $user_id, $username, $name, $password);
			}
			mysqli_close($conn);
			break;

		case 'delete':
			$user_id = $_GET['user_id'];
			$query = "DELETE FROM user WHERE user_id = '$user_id'";
			$execute = mysqli_query($conn, $query);
			if ($execute) {
				header("location:../views/user.php?response=success");
			}
			else {
				header("location:../views/user.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
	}
?>