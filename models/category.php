<?php
	include "connection.php";
	$action = $_GET['action'];

	switch($action) {
		case 'insert':
			$name = $_POST['cname'];
			
			$query = "SELECT * FROM category WHERE name = '$name'";
			$execute = mysqli_query($conn, $query);
			if (mysqli_num_rows($execute) > 0) {
				header("location:../views/category.php?response=failed&message=duplicate");
			}
			else {
				$query = "INSERT INTO category(name) VALUES('$name')";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/category.php?response=success");
				}
				else {
					header("location:../views/category.php?response=failed&message=sql_error");
				}
			}
			mysqli_close($conn);
			break;

		case 'update':
			// Method update
			function update($conn, $category_id, $name) {
				$query = "UPDATE category SET name='$name' WHERE category_id ='$category_id'";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/category.php?response=success");
				}
				else {
					header("location:../views/category.php?response=failed&message=sql_error");
				}
			}

			$category_id = $_POST['e_category_id'];
			$name = $_POST['e_category_name'];
			
			$query = "SELECT * FROM category WHERE name = '$name'";
			$execute = mysqli_query($conn, $query);

			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$current_id = $result['category_id'];
				if ($current_id == $category_id) {
					update($conn, $category_id, $name);
				}
				else {
					header("location:../views/category.php?response=failed&message=duplicate");
				}
			}
			else {
				update($conn, $category_id, $name);
			}
			mysqli_close($conn);
			break;
			
		case 'delete':
			$category_id = $_GET['category_id'];
			$query = "DELETE FROM category WHERE category_id = '$category_id'";
			$execute = mysqli_query($conn, $query);
			if ($execute) {
				header("location:../views/category.php?response=success");
			}
			else {
				header("location:../views/category.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
	}
?>