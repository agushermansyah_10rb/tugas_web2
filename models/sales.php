<?php
	include "connection.php";
	$action = $_GET['action'];

	switch($action) {
		case 'insert':
			$user_id = $_POST['user_id'];
			$product_id = $_POST['product_id'];
			$buyer = $_POST['buyer'];
			$count = $_POST['count'];
			$discount = $_POST['discount'];

			$query = "SELECT * FROM product WHERE product_id = '$product_id'";
			$execute = mysqli_query($conn, $query);
			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$stock = $result["stock"];
				if ($count <= $stock) {
					$new_stock = $stock - $count;
					$query = "INSERT INTO sales(user_id,product_id,buyer_name,count,discount) VALUES('$user_id', '$product_id', '$buyer', '$count', '$discount')";
					$execute = mysqli_query($conn, $query);
					if ($execute) {
						$query_update = "UPDATE product SET stock = '$new_stock' WHERE product_id='$product_id'";
						mysqli_query($conn, $query_update);
						header("location:../views/sales.php?response=success");
					}
					else {
						header("location:../views/sales.php?response=failed&message=sql_error");
					}
				}
				else {
					header("location:../views/sales.php?response=failed&message=limit");
				}
			}
			else {
				header("location:../views/sales.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;

		case 'update':
			// Method update
			function update($conn, $sale_id, $user_id, $product_id, $buyer, $count, $discount, $new_stock) {
				$query = "UPDATE sales SET user_id='$user_id', product_id='$product_id', buyer_name='$buyer', count='$count', discount='$discount' WHERE sale_id ='$sale_id'";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					$query_update = "UPDATE product SET stock = '$new_stock' WHERE product_id='$product_id'";
					mysqli_query($conn, $query_update);
					header("location:../views/sales.php?response=success");
				}
				else {
					header("location:../views/sales.php?response=failed&message=sql_error");
				}
			}

			$sale_id = $_POST['sale_id'];
			$user_id = $_POST['user_id'];
			$product_id = $_POST['product_id'];
			$current_product_id = $_POST['current_product_id'];
			$buyer = $_POST['buyer'];
			$count = $_POST['count'];
			$current_count = $_POST['current_count'];
			$discount = $_POST['discount'];

			$query = "SELECT * FROM product WHERE product_id = '$product_id'";
			$execute = mysqli_query($conn, $query);
			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$stock = $result["stock"];

				if ($current_product_id == $product_id) {
					$new_stock = 0;
					if ($count <= $current_count) {
						// Stock ditambah
						$add_stock = $current_count - $count;
						$new_stock = $stock + $add_stock;
					}
					else {
						// Stock dikurangi
						$min_stock = $count - $current_count;
						$new_stock = $stock - $min_stock;
					}

					if ($new_stock >= 0) {
						// Jika stock masih ada
						update($conn, $sale_id, $user_id, $product_id, $buyer, $count, $discount, $new_stock);
					}
					else {
						header("location:../views/sales.php?response=failed&message=limit");
					}
				}
				else {
					if ($count <= $stock) {
						// Jika stock masih ada
						$new_stock = $stock - $count;
						update($conn, $sale_id, $user_id, $product_id, $buyer, $count, $discount, $new_stock);

						// Update kembali stock product id yg sebelum di update
						$query = "SELECT * FROM product WHERE product_id = '$current_product_id'";
						$execute = mysqli_query($conn, $query);

						if (mysqli_num_rows($execute) > 0) {
							$result = mysqli_fetch_array($execute);
							$stock = $result["stock"];
							$new_stock = $stock + $current_count;
							$query_update = "UPDATE product SET stock = '$new_stock' WHERE product_id='$current_product_id'";
							mysqli_query($conn, $query_update);
						}
					}
					else {
						header("location:../views/sales.php?response=failed&message=limit");
					}
				}
			}
			else {
				header("location:../views/sales.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
			
		case 'delete':
			$sales_id = $_GET['sales_id'];
			$query = "DELETE FROM sales WHERE sale_id = '$sales_id'";
			$execute = mysqli_query($conn, $query);
			if ($execute) {
				header("location:../views/sales.php?response=success");
			}
			else {
				header("location:../views/sales.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
	}
?>