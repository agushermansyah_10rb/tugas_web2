<?php
	include "connection.php";
	$action = $_GET['action'];

	switch($action) {
		case 'insert':
			$name = $_POST['product_name'];
			$size = $_POST['size'];
			$position = $_POST['position'];
			$tube_type = $_POST['tube_type'];
			$ring_number = $_POST['ring_number'];
			$stock = $_POST['stock'];
			$price = $_POST['price'];
			$mn_id = $_POST['md_id'];
			
			$query = "SELECT * FROM product WHERE name = '$name' AND size = '$size' AND position='$position' AND tube_type='$tube_type' AND ring_number='$ring_number' AND mn_id='$mn_id'";
			$execute = mysqli_query($conn, $query);
			if (mysqli_num_rows($execute) > 0) {
				header("location:../views/tire.php?response=failed&message=duplicate");
			}
			else {
				$query = "INSERT INTO product(name,size,position,tube_type,ring_number,stock,price,mn_id) VALUES('$name', '$size', '$position', '$tube_type', '$ring_number', '$stock', '$price', '$mn_id')";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/tire.php?response=success");
				}
				else {
					header("location:../views/tire.php?response=failed&message=sql_error");
				}
			}
			mysqli_close($conn);
			break;

		case 'update':
			// Method update
			function update($conn, $product_id, $name, $size, $position, $tube_type, $ring_number, $stock, $price, $mn_id) {
				$query = "UPDATE product SET name='$name', size='$size', position='$position', tube_type='$tube_type', ring_number='$ring_number', stock='$stock', price='$price', mn_id='$mn_id' WHERE product_id ='$product_id'";
				$execute = mysqli_query($conn, $query);
				if ($execute) {
					header("location:../views/tire.php?response=success");
				}
				else {
					header("location:../views/tire.php?response=failed&message=sql_error");
				}
			}

			$product_id = $_POST['product_id'];
			$name = $_POST['product_name'];
			$size = $_POST['size'];
			$position = $_POST['position'];
			$tube_type = $_POST['tube_type'];
			$ring_number = $_POST['ring_number'];
			$stock = $_POST['stock'];
			$price = $_POST['price'];
			$mn_id = $_POST['md_id'];
			
			$query = "SELECT * FROM product WHERE name = '$name' AND size = '$size' AND position='$position' AND tube_type='$tube_type' AND ring_number='$ring_number' AND mn_id='$mn_id'";
			$execute = mysqli_query($conn, $query);

			if (mysqli_num_rows($execute) > 0) {
				$result = mysqli_fetch_array($execute);
				$current_id = $result['product_id'];
				if ($current_id == $product_id) {
					update($conn, $product_id, $name, $size, $position, $tube_type, $ring_number, $stock, $price, $mn_id);
				}
				else {
					header("location:../views/tire.php?response=failed&message=duplicate");
				}
			}
			else {
				update($conn, $product_id, $name, $size, $position, $tube_type, $ring_number, $stock, $price, $mn_id);
			}
			mysqli_close($conn);
			break;

		case 'delete':
			$product_id = $_GET['product_id'];
			$query = "DELETE FROM product WHERE product_id = '$product_id'";
			$execute = mysqli_query($conn, $query);
			if ($execute) {
				header("location:../views/tire.php?response=success");
			}
			else {
				header("location:../views/tire.php?response=failed&message=sql_error");
			}
			mysqli_close($conn);
			break;
	}
?>