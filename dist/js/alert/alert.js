function success(message, location) {
    swal({
        title: "Success!",
        text: message,
        icon: "success",
        button: "OK",
    })
    .then((value) => {
        // Replace current url
        window.history.replaceState({}, document.title, location);
    });
}

function failed(message, location) {
     swal({
        title: "Failed!",
        text: message,
        icon: "error",
        button: "OK",
    })
    .then((value) => {
        // Replace current url
        window.history.replaceState({}, document.title, location);
    });
}

function ask(location) {
    swal({
        title: "Are you sure?",
        text: "Data will be permanently deleted!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            // Move other url
            window.location.href = location;
        } 
    });
}

function move(location) {
    // Move other url
    window.location.href = location;
}

