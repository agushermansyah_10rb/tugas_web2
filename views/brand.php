<!DOCTYPE html>
<html dir="ltr" lang="en">
<!-- Head -->
<?php include "head.php"; ?>
<!-- Body -->
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- Wrapper -->
    <div id="main-wrapper">
        <!-- Header -->
        <?php include "header.php"; ?>
        <!-- Sidebar -->
        <?php include "sidebar.php"; ?>
        <!-- Page Breadcrumb -->
        <div class="page-wrapper">
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Brand</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../views/dashboard.php">Pusat Ban</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Product</li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="../views/category.php">Category</a></li>
                                    <?php
                                        $merk_id = '';
                                        if ($_GET) {
                                            if (isset($_GET['merk_id'])) {
                                                $merk_id = $_GET['merk_id'];
                                            }
                                        }
                                        $merk_id = 1000;
                                        $query = "SELECT * FROM category_merk WHERE merk_id = '$merk_id'";
                                        $execute = mysqli_query($conn, $query);
                                        $result = mysqli_fetch_array($execute);
                                        $category_id = $result['category_id'];
                                    ?>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="../views/merk.php?category_id=<?php echo $category_id; ?>">Merk</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Brand</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Response status -->
            <?php 
                if ($_GET) {
                    if (isset($_GET['response'])) {
                        $status = $_GET['response'];
                        $merk_id = '';
                        if (isset($_GET['merk_id'])) {
                            $merk_id = $_GET['merk_id'];
                        }
                        if ($status == 'success') {
                            echo "<script type='text/javascript'>success('Data saved!', 'brand.php?merk_id=$merk_id');</script>";
                        } 
                        else if ($status == 'failed') {
                            $message = $_GET['message'];
                            if ($message == 'duplicate') {
                                echo "<script type='text/javascript'>failed('Sorry, data is duplicate!', 'brand.php?merk_id=$merk_id');</script>";
                            }
                            else if ($message == 'sql_error') {
                                echo "<script type='text/javascript'>failed('Sorry, SQL error!', 'brand.php?merk_id=$merk_id');</script>";
                            }
                            else {
                                echo "<script type='text/javascript'>failed('Sorry, Undefined error!', 'brand.php?merk_id=$merk_id');</script>";
                            }
                        }
                    }
                }
            ?>
            <!-- Containter -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <form action="../models/brand.php?action=insert" method="POST">
                                <div class="card-body">
                                    <h4 class="card-title">Add New Brand</h4>
                                    <div class="form-group">
                                        <label for="cname">Brand Name</label>
                                        <?php
                                            $merk_id = '';
                                            if ($_GET) {
                                                if (isset($_GET['merk_id'])) {
                                                    $merk_id = $_GET['merk_id'];
                                                }
                                            }
                                        ?>
                                        <input class="form-control" type="hidden" name="merk_id" value="<?php echo $merk_id; ?>" required="true">
                                        <input class="form-control" type="text" name="bname" required="true">
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <input type="submit" class="btn btn-info" value="Save">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Data Brand</h5>
                                <div class="table-responsive" style="margin-top: 15px;">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Brand ID</th>
                                                <th>Brand Name</th>
                                                <th>Category Name</th>
                                                <th>Merk Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <script type="text/javascript">
                                                function edit(merk_id, brand_id, brand_name) {
                                                    document.getElementById("e_merk_id").value = merk_id;
                                                    document.getElementById("e_brand_id").value = brand_id;
                                                    document.getElementById("e_brand_name").value = brand_name;
                                                }
                                            </script>
                                            <?php 
                                                $merk_id = '';
                                                if ($_GET) {
                                                    if (isset($_GET['merk_id'])) {
                                                        $merk_id = $_GET['merk_id'];
                                                    }
                                                }
                                                $query = "SELECT motor_data.md_id, motor_data.name, category.name as category_name, merk.name as merk_name FROM merk_motor JOIN motor_data ON motor_data.md_id = merk_motor.md_id JOIN merk ON merk.merk_id = merk_motor.merk_id JOIN category_merk ON category_merk.merk_id = merk.merk_id JOIN category ON category.category_id = category_merk.category_id WHERE merk_motor.merk_id = '$merk_id'";
                                                $result = mysqli_query($conn, $query);
                                                while ($rs = mysqli_fetch_array($result)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $rs["md_id"]; ?></td>
                                                <td><?php echo $rs["name"]; ?></td>
                                                <td><?php echo $rs['category_name']; ?></td>
                                                <td><?php echo $rs['merk_name']; ?></td>
                                                <td style="text-align: center;">
                                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit" title="Edit" onclick="edit('<?php echo $merk_id; ?>', '<?php echo $rs["md_id"]; ?>', '<?php echo $rs["name"]; ?>');" style="color: #ffffff;">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm" title="Delete" onclick="ask('../models/brand.php?action=delete&merk_id=<?php echo $merk_id; ?>&brand_id=<?php echo $rs["md_id"]; ?>');" style="color: #ffffff;">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Brand</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="../models/brand.php?action=update" method="POST">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="cname">Brand Name</label>
                                        <input class="form-control" type="hidden" name="e_merk_id" id="e_merk_id" required>
                                        <input class="form-control" type="hidden" name="e_brand_id" id="e_brand_id" required>
                                        <input class="form-control" type="text" name="e_brand_name" id="e_brand_name" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-info" value="Update">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include "footer.php"; ?>
        </div>
    </div>
    <!-- All jqurey -->
    <?php include "foot.php"; ?>
</body>
</html>