<!DOCTYPE html>
<html dir="ltr" lang="en">
<!-- Head -->
<?php include "head.php"; ?>
<!-- Body -->
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- Wrapper -->
    <div id="main-wrapper">
        <!-- Header -->
        <?php include "header.php"; ?>
        <!-- Sidebar -->
        <?php include "sidebar.php"; ?>
        <!-- Page Breadcrumb -->
        <div class="page-wrapper">
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Sales</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../views/dashboard.php">Pusat Ban</a></li>
                                    <li class="breadcrumb-item" aria-current="page">Transaction</li>
                                    <li class="breadcrumb-item active" aria-current="page">Sales</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Response status -->
            <?php 
                if ($_GET) {
                    if (isset($_GET['response'])) {
                        $status = $_GET['response']; 
                        if ($status == 'success') {
                            echo "<script type='text/javascript'>success('Data saved!', 'sales.php');</script>";
                        } 
                        else if ($status == 'failed') {
                            $message = $_GET['message'];
                            if ($message == 'limit') {
                                echo "<script type='text/javascript'>failed('Sorry, Insufficient stock!', 'sales.php');</script>";
                            }
                            else if ($message == 'sql_error') {
                                echo "<script type='text/javascript'>failed('Sorry, SQL error!', 'sales.php');</script>";
                            }
                            else {
                                echo "<script type='text/javascript'>failed('Sorry, Undefined error!', 'sales.php');</script>";
                            }
                        }
                    }
                }
            ?>
            <!-- Containter -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="card-title">Data Sales</h5>
                                    </div>
                                    <div class="col-md-6" style="text-align: right;">
                                         <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#insert">
                                            <i class="fas fa-plus-circle"></i> Add New
                                        </button>
                                    </div>                              
                                </div>
                                <div class="table-responsive" style="margin-top: 15px;">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sales ID</th>
                                                <th>Product Name</th>
                                                <th>Price</th>
                                                <th>Count</th>
                                                <th>Discount</th>
                                                <th>Total</th>
                                                <th>Buyer</th>
                                                <th>Cashier</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <script type="text/javascript">
                                                function edit(sale_id, product_id, buyer, count, discount) {
                                                    document.getElementById("sale_id").value = sale_id;
                                                    document.getElementById("e_product_id").value = product_id;
                                                    document.getElementById("e_current_product_id").value = product_id;
                                                    document.getElementById("e_buyer").value = buyer;
                                                    document.getElementById("e_count").value = count;
                                                    document.getElementById("e_current_count").value = count;
                                                    document.getElementById("e_discount").value = discount;
                                                }
                                            </script>
                                            <?php 
                                                $query = "SELECT user.name as cashier, product.*, sales.* FROM sales JOIN user ON user.user_id = sales.user_id JOIN product ON product.product_id = sales.product_id";
                                                $result = mysqli_query($conn, $query);
                                                while ($rs = mysqli_fetch_array($result)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $rs["sale_id"]; ?></td>
                                                <td><?php echo $rs["name"]; ?></td>
                                                <td><?php echo $rs["price"]; ?></td>
                                                <td><?php echo $rs["count"]; ?></td>
                                                <td><?php echo $rs["discount"]; ?></td>
                                                <td><?php echo $rs["count"] > 0 ? ($rs["price"] * $rs["count"]) - ($rs["price"] * $rs["discount"] / 100) : 0; ?></td>
                                                <td><?php echo $rs["buyer_name"]; ?></td>
                                                <td><?php echo $rs["cashier"]; ?></td>
                                                <td><?php echo $rs["created_at"]; ?></td>
                                                <td style="text-align: center;">
                                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit" title="Edit" onclick="edit('<?php echo $rs["sale_id"]; ?>', '<?php echo $rs["product_id"]; ?>', '<?php echo $rs["buyer_name"]; ?>', '<?php echo $rs["count"]; ?>', '<?php echo $rs["discount"]; ?>');" style="color: #ffffff;">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm" title="Delete" onclick="ask('../models/sales.php?action=delete&sales_id=<?php echo $rs["sale_id"]; ?>');" style="color: #ffffff;">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div id="insert" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add Sales</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="../models/sales.php?action=insert" method="POST">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input class="form-control" type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['user_id']; ?>" required>
                                        <label for="product_name">Product Name</label>
                                         <select name="product_id" class="select2 form-control custom-select" onchange="change();" required>
                                            <option>Select</option>
                                            <optgroup label="Product">
                                                  <?php 
                                                    $query = "SELECT * FROM product";
                                                    $result = mysqli_query($conn, $query);
                                                    while ($rs = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value="<?php echo $rs['product_id']; ?>" data-content="<?php print_r($rs); ?>"><?php echo $rs["name"]; ?></option>
                                                <?php } ?>
                                            </optgroup>
                                        </select>
                                        <label for="price">Buyer</label>
                                        <input class="form-control" type="text" name="buyer" id="buyer" required>
                                        <label for="price">Count</label>
                                        <input class="form-control" type="text" name="count" id="count" required>
                                        <label for="price">Discount</label>
                                        <input class="form-control" type="text" name="discount" id="discount" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-info" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="edit" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Sales</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="../models/sales.php?action=update" method="POST">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input class="form-control" type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['user_id']; ?>" required>
                                        <input class="form-control" type="hidden" name="sale_id" id="sale_id" required>
                                        <input class="form-control" type="hidden" name="current_product_id" id="e_current_product_id" required>
                                        <label for="product_name">Product Name</label>
                                         <select name="product_id" id="e_product_id" class="select2 form-control custom-select" onchange="change();" required>
                                            <option>Select</option>
                                            <optgroup label="Product">
                                                  <?php 
                                                    $query = "SELECT * FROM product";
                                                    $result = mysqli_query($conn, $query);
                                                    while ($rs = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value="<?php echo $rs['product_id']; ?>" data-content="<?php print_r($rs); ?>"><?php echo $rs["name"]; ?></option>
                                                <?php } ?>
                                            </optgroup>
                                        </select>
                                        <label for="price">Buyer</label>
                                        <input class="form-control" type="text" name="buyer" id="e_buyer" required>
                                        <label for="price">Count</label>
                                        <input class="form-control" type="text" name="count" id="e_count" required>
                                        <input class="form-control" type="hidden" name="current_count" id="e_current_count" required>
                                        <label for="price">Discount</label>
                                        <input class="form-control" type="text" name="discount" id="e_discount" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-info" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include "footer.php"; ?>
        </div>
    </div>
    <!-- All jqurey -->
    <?php include "foot.php"; ?>
</body>
</html>