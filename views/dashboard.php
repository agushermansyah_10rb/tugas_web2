<!DOCTYPE html>
<html dir="ltr" lang="en">

<?php include "head.php"; ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include "header.php"; ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include "sidebar.php"; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Pusat Ban</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <br><br><br>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <div class="card card-hover">
                            <a href="category.php">
                                <div class="box bg-warning" style="height: 120px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-format-list-bulleted" style="font-size: 45pt"></i></h1>
                                    <h6 class="text-white text-right">Category</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a href="tire.php">
                           <div class="card card-hover">
                                <div class="box bg-danger" style="height: 120px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-disk" style="font-size: 45pt"></i></h1>
                                    <h6 class="text-white text-right">Tire</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2">
                        <a href="Sales.php">
                            <div class="card card-hover">
                                <div class="box bg-info" style="height: 120px;">
                                    <h1 class="font-light text-white"><i class="fas fa-shopping-cart" style="font-size: 45pt"></i></h1>
                                    <h6 class="text-white text-right">Sales</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <a href="purchasing.php">
                            <div class="card card-hover">
                                <div class="box bg-cyan" style="height: 120px;">
                                    <h1 class="font-light text-white"><i class="fas fa-shopping-bag" style="font-size: 45pt"></i></h1>
                                    <h6 class="text-white text-right">Purchasing</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="supplier.php">
                           <div class="card card-hover">
                                <div class="box bg-success" style="height: 120px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-account-multiple" style="font-size: 45pt"></i></h1>
                                    <h6 class="text-white text-right">Supplier</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="user.php">
                            <div class="card card-hover">
                                <div class="box bg-warning" style="height: 120px;">
                                    <h1 class="font-light text-white"><i class="mdi mdi-account-circle" style="font-size: 45pt"></i></h1>
                                    <h6 class="text-white text-right">User</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include "footer.php"; ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php include "foot.php"; ?>
</body>

</html>