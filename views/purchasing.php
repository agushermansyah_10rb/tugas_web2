<!DOCTYPE html>
<html dir="ltr" lang="en">
<!-- Head -->
<?php include "head.php"; ?>
<!-- Body -->
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- Wrapper -->
    <div id="main-wrapper">
        <!-- Header -->
        <?php include "header.php"; ?>
        <!-- Sidebar -->
        <?php include "sidebar.php"; ?>
        <!-- Page Breadcrumb -->
        <div class="page-wrapper">
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Purchasing</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../views/dashboard.php">Pusat Ban</a></li>
                                    <li class="breadcrumb-item" aria-current="page">Transaction</li>
                                    <li class="breadcrumb-item active" aria-current="page">Purchasing</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Response status -->
            <?php 
                if ($_GET) {
                    if (isset($_GET['response'])) {
                        $status = $_GET['response']; 
                        if ($status == 'success') {
                            echo "<script type='text/javascript'>success('Data saved!', 'purchasing.php');</script>";
                        } 
                        else if ($status == 'failed') {
                            $message = $_GET['message'];
                            if ($message == 'duplicate') {
                                echo "<script type='text/javascript'>failed('Sorry, data is duplicate!', 'purchasing.php');</script>";
                            }
                            else if ($message == 'sql_error') {
                                echo "<script type='text/javascript'>failed('Sorry, SQL error!', 'purchasing.php');</script>";
                            }
                            else {
                                echo "<script type='text/javascript'>failed('Sorry, Undefined error!', 'purchasing.php');</script>";
                            }
                        }
                    }
                }
            ?>
            <!-- Containter -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="card-title">Data Purchasing</h5>
                                    </div>
                                    <div class="col-md-6" style="text-align: right;">
                                         <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#insert">
                                            <i class="fas fa-plus-circle"></i> Add New
                                        </button>
                                    </div>                              
                                </div>
                                <div class="table-responsive" style="margin-top: 15px;">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Product ID</th>
                                                <th>Name</th>
                                                <th>Size</th>
                                                <th>Position</th>
                                                <th>Tube</th>
                                                <th>Ring Number</th>
                                                <th>Total</th>
                                                <th>Price</th>
                                                <th>Supplier</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <script type="text/javascript">
                                                function edit(purchasing_id, name, size, position, tube_type, ring_number, total, price, supplier_id) {
                                                    document.getElementById("e_purchasing_id").value = purchasing_id;
                                                    document.getElementById("e_product_name").value = name;
                                                    document.getElementById("e_size").value = size;
                                                    document.getElementById("e_position").value = position;
                                                    document.getElementById("e_ring_number").value = ring_number;
                                                    document.getElementById("e_tube_type").value = tube_type;
                                                    document.getElementById("e_total").value = total;
                                                    document.getElementById("e_price").value = price;
                                                    document.getElementById("e_supplier_id").value = supplier_id;
                                                }
                                            </script>
                                            <?php 
                                                $query = "SELECT supplier.name as supplier_name, purchasing.* FROM purchasing JOIN supplier ON purchasing.supplier_id = supplier.supplier_id";
                                                $result = mysqli_query($conn, $query);
                                                while ($rs = mysqli_fetch_array($result)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $rs["purchasing_id"]; ?></td>
                                                <td><?php echo $rs["product_name"]; ?></td>
                                                <td><?php echo $rs["size"]; ?></td>
                                                <td><?php echo $rs["position"]; ?></td>
                                                <td><?php echo $rs["tube_type"]; ?></td>
                                                <td><?php echo $rs["ring_number"]; ?></td>
                                                <td><?php echo $rs["total"]; ?></td>
                                                <td><?php echo $rs["price"]; ?></td>
                                                <td><?php echo $rs["supplier_name"]; ?></td>
                                                <td style="text-align: center;">
                                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit" title="Edit" onclick="edit('<?php echo $rs["purchasing_id"]; ?>', '<?php echo $rs["product_name"]; ?>', '<?php echo $rs["size"]; ?>', '<?php echo $rs["position"]; ?>', '<?php echo $rs["tube_type"]; ?>', '<?php echo $rs["ring_number"]; ?>', '<?php echo $rs["total"]; ?>', '<?php echo $rs["price"]; ?>', '<?php echo $rs["supplier_id"]; ?>');" style="color: #ffffff;">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm" title="Delete" onclick="ask('../models/purchasing.php?action=delete&purchasing_id=<?php echo $rs["purchasing_id"]; ?>');" style="color: #ffffff;">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div id="insert" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add Purchasing</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="../models/purchasing.php?action=insert" method="POST">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="product_name">Product Name</label>
                                        <input class="form-control" type="text" name="product_name" id="product_name" required>
                                        <label for="size">Size</label>
                                        <input class="form-control" type="text" name="size" id="size" required>
                                        <label for="position">Position</label>
                                        <select name="position" class="select2 form-control custom-select" required>
                                            <option>Select</option>
                                            <optgroup label="Position">
                                                <option value="F">F</option>
                                                <option value="R">R</option>
                                                <option value="FR">FR</option>
                                            </optgroup>
                                        </select>
                                        <label for="tube_type">Tube Type</label>
                                        <select name="tube_type" class="select2 form-control custom-select" required>
                                            <option>Select</option>
                                            <optgroup label="Tube Type">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </optgroup>
                                        </select>
                                        <label for="ring_number">Ring Number</label>
                                        <input class="form-control" type="text" name="ring_number" id="ring_number" required>
                                        <label for="stock">Total</label>
                                        <input class="form-control" type="text" name="total" id="total" required>
                                        <label for="price">Price</label>
                                        <input class="form-control" type="text" name="price" id="price" required>
                                        <label for="price">Supplier</label>
                                        <select name="supplier_id" class="select2 form-control custom-select" required>
                                            <option>Select</option>
                                            <optgroup label="Supplier">
                                                <?php 
                                                    $query = "SELECT * FROM supplier";
                                                    $result = mysqli_query($conn, $query);
                                                    while ($rs = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value="<?php echo $rs['supplier_id']; ?>"><?php echo $rs["name"]; ?></option>
                                                <?php } ?>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-info" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="edit" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Purchasing</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="../models/purchasing.php?action=update" method="POST">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input class="form-control" type="hidden" name="purchasing_id" id="e_purchasing_id" required>
                                        <label for="product_name">Product Name</label>
                                        <input class="form-control" type="text" name="product_name" id="e_product_name" required>
                                        <label for="size">Size</label>
                                        <input class="form-control" type="text" name="size" id="e_size" required>
                                        <label for="position">Position</label>
                                        <select name="position" class="select2 form-control custom-select" id="e_position" required>
                                            <option>Select</option>
                                            <optgroup label="Position">
                                                <option value="F">F</option>
                                                <option value="R">R</option>
                                                <option value="FR">FR</option>
                                            </optgroup>
                                        </select>
                                        <label for="tube_type">Tube Type</label>
                                        <select name="tube_type" class="select2 form-control custom-select" id="e_tube_type" required>
                                            <option>Select</option>
                                            <optgroup label="Tube Type">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </optgroup>
                                        </select>
                                        <label for="ring_number">Ring Number</label>
                                        <input class="form-control" type="text" name="ring_number" id="e_ring_number" required>
                                        <label for="stock">Total</label>
                                        <input class="form-control" type="text" name="total" id="e_total" required>
                                        <label for="price">Price</label>
                                        <input class="form-control" type="text" name="price" id="e_price" required>
                                        <label for="price">Supplier</label>
                                        <select name="supplier_id" class="select2 form-control custom-select" id="e_supplier_id" required>
                                            <option>Select</option>
                                            <optgroup label="Supplier">
                                                <?php 
                                                    $query = "SELECT * FROM supplier";
                                                    $result = mysqli_query($conn, $query);
                                                    while ($rs = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value="<?php echo $rs['supplier_id']; ?>"><?php echo $rs["name"]; ?></option>
                                                <?php } ?>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-info" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include "footer.php"; ?>
        </div>
    </div>
    <!-- All jqurey -->
    <?php include "foot.php"; ?>
</body>
</html>