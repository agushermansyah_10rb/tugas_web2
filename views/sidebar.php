<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard.php" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-cube"></i><span class="hide-menu">Product</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="category.php" class="sidebar-link"><i class="mdi mdi-format-list-bulleted"></i><span class="hide-menu"> Category </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="tire.php" class="sidebar-link"><i class="mdi mdi-disk"></i><span class="hide-menu"> Tire </span></a>
                        </li>

                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-file-send"></i><span class="hide-menu">Transaction</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="sales.php" class="sidebar-link"><i class="fas fa-shopping-cart"></i><span class="hide-menu"> Sales </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="purchasing.php" class="sidebar-link"><i class="fas fa-shopping-bag"></i><span class="hide-menu"> Purchasing </span></a>
                        </li>

                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="supplier.php" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Supplier</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="user.php" aria-expanded="false"><i class="mdi mdi-account-circle"></i><span class="hide-menu">User</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="../models/logout.php" aria-expanded="false"><i class="mdi mdi-logout"></i><span class="hide-menu">Logout</span></a></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>