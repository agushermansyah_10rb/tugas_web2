<!DOCTYPE html>
<html dir="ltr" lang="en">
<!-- Head -->
<?php include "head.php"; ?>
<!-- Body -->
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- Wrapper -->
    <div id="main-wrapper">
        <!-- Header -->
        <?php include "header.php"; ?>
        <!-- Sidebar -->
        <?php include "sidebar.php"; ?>
        <!-- Page Breadcrumb -->
        <div class="page-wrapper">
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">User</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../views/dashboard.php">Pusat Ban</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">User</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Response status -->
            <?php 
                if ($_GET) {
                    if (isset($_GET['response'])) {
                        $status = $_GET['response']; 
                        if ($status == 'success') {
                            echo "<script type='text/javascript'>success('Data saved!', 'user.php');</script>";
                        } 
                        else if ($status == 'failed') {
                            $message = $_GET['message'];
                            if ($message == 'duplicate') {
                                echo "<script type='text/javascript'>failed('Sorry, data is duplicate!', 'user.php');</script>";
                            }
                            else if ($message == 'sql_error') {
                                echo "<script type='text/javascript'>failed('Sorry, SQL error!', 'user.php');</script>";
                            }
                            else {
                                echo "<script type='text/javascript'>failed('Sorry, Undefined error!', 'user.php');</script>";
                            }
                        }
                    }
                }
            ?>
            <!-- Containter -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <form action="../models/user.php?action=insert" method="POST">
                                <div class="card-body">
                                    <h4 class="card-title">Add New User</h4>
                                    <div class="form-group">
                                        <label for="supplier_name">Display Name</label>
                                        <input class="form-control" type="text" name="name" required="true">
                                        <label for="address">Username</label>
                                        <input class="form-control" type="text" name="username" required="true">
                                        <label for="address">Password</label>
                                        <input class="form-control" type="password" name="password" required="true">
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <input type="submit" class="btn btn-info" value="Save">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Data User</h5>
                                <div class="table-responsive" style="margin-top: 15px;">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>User ID</th>
                                                <th>Username</th>
                                                <th>Display Name</th>
                                                <th>Password</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <script type="text/javascript">
                                                function edit(user_id, username, name, password) {
                                                    document.getElementById("e_user_id").value = user_id;
                                                    document.getElementById("e_username").value = username;
                                                    document.getElementById("e_display_name").value = name;
                                                    document.getElementById("e_password").value = password;
                                                }
                                            </script>
                                            <?php 
                                                $query = "SELECT * FROM user";
                                                $result = mysqli_query($conn, $query);
                                                while ($rs = mysqli_fetch_array($result)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $rs["user_id"]; ?></td>
                                                <td><?php echo $rs["username"]; ?></td>
                                                <td><?php echo $rs["name"]; ?></td>
                                                <td><?php echo $rs["password"]; ?></td>
                                                <td style="text-align: center;">
                                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit" title="Edit" onclick="edit('<?php echo $rs["user_id"]; ?>', '<?php echo $rs["username"]; ?>', '<?php echo $rs["name"]; ?>', '<?php echo $rs["password"]; ?>');" style="color: #ffffff;">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm" title="Delete" onclick="ask('../models/user.php?action=delete&user_id=<?php echo $rs["user_id"]; ?>');" style="color: #ffffff;">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit User</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="../models/user.php?action=update" method="POST">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input class="form-control" type="hidden" name="e_user_id" id="e_user_id" required>
                                        <label for="e_display_name">Display Name</label>
                                        <input class="form-control" type="text" name="e_display_name" id="e_display_name" required>
                                        <label for="e_username">Username</label>
                                        <input class="form-control" type="text" name="e_username" id="e_username" required>
                                        <label for="e_password">Password</label>
                                        <input class="form-control" type="password" name="e_password" id="e_password" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-info" value="Update">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include "footer.php"; ?>
        </div>
    </div>
    <!-- All jqurey -->
    <?php include "foot.php"; ?>
</body>
</html>