<!DOCTYPE html>
<html dir="ltr" lang="en">
<!-- Head -->
<?php include "head.php"; ?>
<!-- Body -->
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- Wrapper -->
    <div id="main-wrapper">
        <!-- Header -->
        <?php include "header.php"; ?>
        <!-- Sidebar -->
        <?php include "sidebar.php"; ?>
        <!-- Page Breadcrumb -->
        <div class="page-wrapper">
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Merk</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="../views/dashboard.php">Pusat Ban</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Product</li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="../views/category.php">Category</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Merk</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Response status -->
            <?php 
                if ($_GET) {
                    if (isset($_GET['response'])) {
                        $status = $_GET['response'];
                        $category_id = '';
                        if (isset($_GET['category_id'])) {
                            $category_id = $_GET['category_id'];
                        }
                        if ($status == 'success') {
                            echo "<script type='text/javascript'>success('Data saved!', 'merk.php?category_id=$category_id');</script>";
                        } 
                        else if ($status == 'failed') {
                            $message = $_GET['message'];
                            if ($message == 'duplicate') {
                                echo "<script type='text/javascript'>failed('Sorry, data is duplicate!', 'merk.php?category_id=$category_id');</script>";
                            }
                            else if ($message == 'sql_error') {
                                echo "<script type='text/javascript'>failed('Sorry, SQL error!', 'merk.php?category_id=$category_id');</script>";
                            }
                            else {
                                echo "<script type='text/javascript'>failed('Sorry, Undefined error!', 'merk.php?category_id=$category_id');</script>";
                            }
                        }
                    }
                }
            ?>
            <!-- Containter -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <form action="../models/merk.php?action=insert" method="POST">
                                <div class="card-body">
                                    <h4 class="card-title">Add New Merk</h4>
                                    <div class="form-group">
                                        <label for="cname">Merk Name</label>
                                        <?php
                                            $category_id = '';
                                            if ($_GET) {
                                                if (isset($_GET['category_id'])) {
                                                    $category_id = $_GET['category_id'];
                                                }
                                            }
                                        ?>
                                        <input class="form-control" type="hidden" name="category_id" value="<?php echo $category_id; ?>" required="true">
                                        <input class="form-control" type="text" name="mname" required="true">
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <input type="submit" class="btn btn-info" value="Save">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Data Merk</h5>
                                <div class="table-responsive" style="margin-top: 15px;">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Merk ID</th>
                                                <th>Merk Name</th>
                                                <th>Category Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <script type="text/javascript">
                                                function edit(category_id, merk_id, merk_name) {
                                                    document.getElementById("e_category_id").value = category_id;
                                                    document.getElementById("e_merk_id").value = merk_id;
                                                    document.getElementById("e_merk_name").value = merk_name;
                                                }
                                            </script>
                                            <?php 
                                                $category_id = '';
                                                if ($_GET) {
                                                    if (isset($_GET['category_id'])) {
                                                        $category_id = $_GET['category_id'];
                                                    }
                                                }
                                                $query = "SELECT merk.merk_id, merk.name, category.name as category_name FROM category_merk JOIN merk ON merk.merk_id = category_merk.merk_id JOIN category ON category.category_id = category_merk.category_id WHERE category_merk.category_id = '$category_id'";
                                                $result = mysqli_query($conn, $query);
                                                while ($rs = mysqli_fetch_array($result)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $rs["merk_id"]; ?></td>
                                                <td><?php echo $rs["name"]; ?></td>
                                                <td><?php echo $rs['category_name']; ?></td>
                                                <td style="text-align: center;">
                                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit" title="Edit" onclick="edit('<?php echo $category_id; ?>', '<?php echo $rs["merk_id"]; ?>', '<?php echo $rs["name"]; ?>');" style="color: #ffffff;">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm" title="Delete" onclick="ask('../models/merk.php?action=delete&category_id=<?php echo $category_id; ?>&merk_id=<?php echo $rs["merk_id"]; ?>');" style="color: #ffffff;">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-info btn-sm" title="Detail" onclick="move('../views/brand.php?merk_id=<?php echo $rs['merk_id']; ?>')" style="color: #ffffff;">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div id="edit" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Edit Merk</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form action="../models/merk.php?action=update" method="POST">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="cname">Merk Name</label>
                                        <input class="form-control" type="hidden" name="e_category_id" id="e_category_id" required>
                                        <input class="form-control" type="hidden" name="e_merk_id" id="e_merk_id" required>
                                        <input class="form-control" type="text" name="e_merk_name" id="e_merk_name" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-info" value="Update">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include "footer.php"; ?>
        </div>
    </div>
    <!-- All jqurey -->
    <?php include "foot.php"; ?>
</body>
</html>